package pl.klasy2Zadanie10;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Point2D point1=new Point2D();
        Point2D point2=new Point2D(1,3);
        Point3D point3=new Point3D();
        Point3D point4=new Point3D(8);
        Point3D point5=new Point3D(2,5,7);


        System.out.println(point1);
        System.out.println(point2);
        System.out.println(point3);
        System.out.println(point4);
        System.out.println(point5);
    }
}
