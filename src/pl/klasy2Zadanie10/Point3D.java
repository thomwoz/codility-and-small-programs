package pl.klasy2Zadanie10;

public class Point3D extends Point2D {
    private int z;

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public Point3D(int z) {
        super();
        this.z = z;
    }

    public Point3D(){
        super();
        z=0;
    }

    @Override
    public String toString() {
        return super.toString() + "Point3D{" +
                "z=" + z +
                '}';
    }
}
