package pl.klasyzadania2;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private String surname;
    private String gender;
    private int age;

    private static List<Person> people = new ArrayList<>();

    public Person(String name, String surname, String gender, int age) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.age = age;
        people.add(this);
    }

    public String getName() {
        return name;
    }

    public static List<Person> getAllPeople() {
        return people;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                '}';
    }

}
