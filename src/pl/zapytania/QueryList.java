package pl.zapytania;

import java.util.ArrayList;
import java.util.List;

public class QueryList {

    private List<Query> queryList;

    public QueryList() {
        this.queryList = new ArrayList<>();
    }

    public List<Query> getQueryList() {
        return queryList;
    }

    public void setQueryList(List<Query> queryList) {
        this.queryList = queryList;
    }

}
