package pl.zapytania;

import java.time.LocalDate;

public class Query {

    private String queryAuthor;
    private String email;
    private LocalDate createdAt;

    public Query(String queryAuthor, String email, LocalDate createdAt) {
        this.queryAuthor = queryAuthor;
        this.email = email;
        this.createdAt=createdAt;
    }

    public String getQueryAuthor() {
        return queryAuthor;

    }

    public void setQueryAuthor(String queryAuthor) {
        this.queryAuthor = queryAuthor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate date) {
        this.createdAt = date;
    }

    public boolean isOlderThan2Weeks() {
        return (createdAt.plusDays(14).isBefore(LocalDate.now()));
    }
}
