package pl.zapytania;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj datę: ");
        String date = scanner.nextLine();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        LocalDate dateTime = LocalDate.parse(date, formatter);
        System.out.println(dateTime);

        System.out.println("Podaj email: ");
        String email= scanner.nextLine();
        System.out.println("Kto stworzył? ");
        String name=scanner.nextLine();

        Query query= new Query(name, email, dateTime);
        if(query.isOlderThan2Weeks()) System.out.println("Starsze niż 2 tygodnie.");
        else System.out.println("Nie jest starsze. ");


    }
}
