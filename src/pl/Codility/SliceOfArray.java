package pl.Codility;

public class SliceOfArray {

    static int counter = 1;
    static int currentCounter = 1;

    static int solution(int[] A) {

        int P = 0;
        for (int i = 0; i < A.length - 1; i++) {
            System.out.println(" ");
            System.out.println(" ");
            System.out.println("ITERACJA NR " + i);
            if (A[i + 1] < A[i]) lookForLargestSlice(A, i, i + 1, -1);

            else if(A[i+1]> A[i]) lookForLargestSlice(A, i, i + 1, 1);
            if(currentCounter>counter) {
                counter=currentCounter;
            }
            currentCounter=1;
            System.out.println("currentCounter= "+currentCounter + " counter="+counter);
        }
            if(counter>2) return counter;
        else return 0;

    }

    static boolean lookForLargestSlice(int[] A, int P, int Q, int lookForLessOrMore) {
        if (Q == A.length - 1) return true;
        if (lookForLessOrMore == -1) {
            if (A[P] < A[Q]) {
                System.out.println("A[p]<A[Q]" + A[P] + " < " + A[Q] + "false");
                return false;
            }
            else if(A[P]==A[Q]) return false;
            else {
                System.out.println("A[p]>A[Q]" + A[P] + " >" + A[Q] + "true");
                currentCounter++;
                System.out.println("currentCounter= " + currentCounter);
                return lookForLargestSlice(A, P + 1, Q + 1, 1);

            }
        } else {
            if (A[P] > A[Q]) {
                System.out.println("A[p]>A[Q]" + A[P] + " > " + A[Q] + "false");
                return false;
            }
            else if(A[P]==A[Q]) return false;
            else {
                System.out.println("A[p]<A[Q]" + A[P] + " < " + A[Q] + "true");
                currentCounter++;
                System.out.println("currentCounter= " + currentCounter);
                return lookForLargestSlice(A, P + 1, Q + 1, -1);
            }
        }
    }

    public static void main(String[] args) {
        int[] A={8,1,-5,1,1,1,3,4,1,8,8,8,8,8,7};

        int z=solution(A);
        System.out.println("Najdłuższy podciąg ma: " + z);
    }
}
