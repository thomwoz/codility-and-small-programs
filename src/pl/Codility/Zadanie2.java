package pl.Codility;

public class Zadanie2 {

    static String solution(String s) {
        String telNumber = new String(s.replaceAll(" ", "").replaceAll("-", ""));
        StringBuilder stringBuilder = new StringBuilder("");
        System.out.println(telNumber);
        for (int i = 0; i < telNumber.length(); i++) {

                if (i!=0 && (i) % 3 == 0) stringBuilder.append("-");
            stringBuilder.append(telNumber.charAt(i));
        }
        if(telNumber.length()%3==1) {
            char a=stringBuilder.charAt(stringBuilder.length()-2);
            char b=stringBuilder.charAt(stringBuilder.length()-3);
            stringBuilder.setCharAt(stringBuilder.length()-2,b);
            stringBuilder.setCharAt(stringBuilder.length()-3,a);
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        String s="0 - 22 1985--324";
        System.out.println(solution(s));
    }
}
