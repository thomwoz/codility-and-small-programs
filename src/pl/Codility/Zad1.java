package pl.Codility;

public class Zad1 {
    static int solution(int a, int b) {
        double sqrt;
        int counter=0;
        for(int i=a; i<=b; i++) {
            sqrt=Math.sqrt(i);
            if((int)sqrt*sqrt==i) counter++;
        }

   /*     //łatwiejsze rozwiązanie
        double sqrt1 = Math.sqrt(a);
        double sqrt2 = Math.sqrt(b);

        //return (int) (Math.floor(Math.sqrt(b))-Math.ceil(Math.sqrt(a)) + 1 );

        counter = (int) sqrt2 - (int) sqrt1;
        //*/

        return counter;
    }

    public static void main(String[] args) {
        int a=5; int b=25;
        System.out.println(solution(a,b));
    }
}
