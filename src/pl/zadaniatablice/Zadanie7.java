package pl.zadaniatablice;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie7 {
    private static boolean doesNumberRepeatItselfMoreThan3Time(int[] array) {
        int z=0;
        System.out.println("Długość tablicy: "+ array.length);
        Arrays.sort(array);
        int repetitionCounter=1;
        int repeatedNumber;


        while (z<array.length) {
            repeatedNumber=array[z];
            z++;
            while(z<array.length && repeatedNumber==array[z]){
                repetitionCounter++;
                if(repetitionCounter>=3) return true;
                z++;
            }

            repetitionCounter=1;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wczytaj wielkość tablicy:");
        int arraySize = scanner.nextInt();
        int[] array = new int[arraySize];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Podaj wartość nr " + (i + 1));
            array[i] = scanner.nextInt();
        }

        if(doesNumberRepeatItselfMoreThan3Time(array)) System.out.println("TAK");
        else System.out.println("NIE");


    }
}
