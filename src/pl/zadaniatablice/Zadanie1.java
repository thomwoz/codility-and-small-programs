package pl.zadaniatablice;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        System.out.println("Wprowadź n: ");
        n=scanner.nextInt();
        int[] tab = new int[n];
        for(int i=0; i<n; i++) {

            System.out.println("Wprowadź liczbę: ");
            tab[i]=scanner.nextInt();

        }

        for(int i=n-1; i>=0; i--){
            System.out.println("Tab["+i+"] = "+tab[i]);
        }
    }
}
