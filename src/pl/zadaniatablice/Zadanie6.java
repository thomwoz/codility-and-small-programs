package pl.zadaniatablice;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Wczytaj wielkość tablicy:");
        int arraySize=scanner.nextInt();
        int[] array=new int[arraySize];
        int b1sum=0;
        for(int i=0; i<array.length; i++) {
            System.out.println("Podaj wartość nr " + (i+1));
            array[i]=scanner.nextInt();
            b1sum+=array[i];
        }

        int[] arrayOfSums=new int[arraySize];
        arrayOfSums[0]=b1sum;
        for(int i=1; i<arraySize; i++) {
            arrayOfSums[i]=arrayOfSums[i-1]-array[i-1];
        }

        for(int i=0; i<arraySize; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
        for(int i=0; i<arraySize; i++) {
            System.out.print(arrayOfSums[i] + " ");
        }
    }
}
