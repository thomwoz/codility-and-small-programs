package pl.zadaniatablice;

import java.util.ArrayList;
import java.util.List;

public class Zadanie11 {
    public int[] solution(int[] A, int K) {
        List<Integer> list = new ArrayList<>();
        for (int index = 0; index < A.length; index++)
        {
            list.add(A[index]);
        }

        int[] newArray=new int[A.length];

        for(int i=0; i<A.length; i++) {
            newArray[(i+K)%A.length]=list.get(i);
        }

        return newArray;


    }
}
