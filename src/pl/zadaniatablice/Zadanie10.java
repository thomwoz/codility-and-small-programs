package pl.zadaniatablice;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie10 {
    private static int[][] createAnArray(){
        System.out.println("Podaj wymiary tablicy: ");
        Scanner scanner=new Scanner(System.in);
        int x=scanner.nextInt();
        int y=scanner.nextInt();
        int[][] array=new int[x][y];
        for(int i=0; i<array.length; i++){
            for(int j=0; j<array[i].length; j++){
                System.out.println("Podaj wartość ["+i+"]["+j+"]");
                array[i][j]=scanner.nextInt();
            }
        }
        return array;
    }

    private static int[] sumOfArray(int[][] array){
        int[]sums=new int[array.length];

        for(int i=0; i<array.length; i++) {
            int sum=0;
            for(int j=0; j<array[i].length; j++) {
                sum+=array[i][j];
            }
            sums[i]=sum;
        }

        return sums;
    }

    private static int[][] findMinAndMax(int[][] array) {
        int[][] minAndMax = new int[array.length][2];
        for(int i=0; i<array.length; i++) {
            Arrays.sort(array[i]);
            minAndMax[i][0]=array[i][0];
            minAndMax[i][1]=array[i][array.length-1];
        }
        return minAndMax;
    }

    public static void main(String[] args) {
        int[][] array=createAnArray();

        int[] sum=sumOfArray(array);
        System.out.println("Sumy wynoszą: ");
        for(int i=0; i<sum.length; i++) {
            System.out.println(i + " - " + sum[i]);
        }

        int[][] minAndMax=findMinAndMax(array);
        for(int i=0; i<minAndMax.length;i++){

            System.out.println("Wiersz nr "+ i + " min = "+ minAndMax[i][0]+ " max = " + minAndMax[i][1]);

        }
    }
}
