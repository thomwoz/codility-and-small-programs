package pl.zadaniatablice;

import java.util.Scanner;

public class Zadanie9 {

    private static int numberOfRepeatingK (int[] array, int k) {
        int count=0;
        for(int i=0; i<array.length; i++){
            if(array[i]==k) count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int[] array={1,3,2,3,4,5,7,44,3,2,55,6,55,6,3,4,99,2,11};
        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int k=scanner.nextInt();
        System.out.println("Liczba "+ k + " występuje "+ numberOfRepeatingK(array,k)+ " razy.");
    }
}
