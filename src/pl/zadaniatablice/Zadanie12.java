package pl.zadaniatablice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zadanie12 {
    public int solution(int[] A) {
        Arrays.sort(A);
        for(int i=0; i<A.length; i++) {
            if(A[i]==-1) continue;
            for(int j=i+1; j<A.length; j++){
                if(A[j]==-1) continue;
                if(A[i]==A[j]) {
                    A[i]=-1;
                    A[j]=-1;
                    break;
                }
            }
        }
        int value=0;
        for(int i=0; i<A.length; i++) {
            if(A[i]!=-1) value=A[i];
        }
        return value;
    }
}
