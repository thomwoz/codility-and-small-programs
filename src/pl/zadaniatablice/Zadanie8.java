package pl.zadaniatablice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Zadanie8 {
    private static int howManyDifferentModuloRests(int[] array) {
        Map<Integer, String> map = new HashMap<>();
        for(int i=0; i<array.length; i++) {
            if(!map.containsKey(array[i]%37)) map.put(array[i]%37, "Rest");
        }
        return map.size();

    }

    private static int howMany(int[] array) {
        Set<Integer> set = new HashSet<>();
        for(int i=0; i<array.length; i++) {
            if(!set.contains(array[i]%37)) set.add(array[i]%37);
        }
        return set.size();
    }

    private static int howManyUsingArray(int[] array){
        int[] arrayOfDivisionRest = new int[array.length];
        for(int i=0; i<array.length; i++){
            arrayOfDivisionRest[i]=-1;
        }
        boolean tmp=false;
        for (int i=0; i<array.length; i++){

            int z=0;
            while(z<array.length && arrayOfDivisionRest[z]!=-1){
                System.out.println("ttt");
                if(arrayOfDivisionRest[z]==array[i]%37) {
                    tmp=true;
                    break;
                }
                z++;
            }
            if(!tmp) arrayOfDivisionRest[z]=array[i]%37;
        }
        int countDifferentRests = 0;
        int z=0;
        while(z<arrayOfDivisionRest.length&& arrayOfDivisionRest[z]!=-1){
            countDifferentRests++;
        }
        return countDifferentRests;
    }

    public static void main(String[] args) {
        int[] array={1,
                38,
                39,
                75,
                76,
                77,
                112,
                113,
                114,
                115,
                149,
                150,
                151,
                153,
                154};

        System.out.println(howManyDifferentModuloRests(array));
        System.out.println(howMany(array));
        System.out.println(howManyUsingArray(array));
    }


}
