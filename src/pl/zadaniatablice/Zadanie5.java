package pl.zadaniatablice;

import java.util.Arrays;

public class Zadanie5 {
    public static void main(String[] args) {
        int[] array={4,3,2,5,15,16,12,45,34,67,-4,-11,4,5,3,4,6,6,2,11,-11,155,11,11,11,11};
        Arrays.sort(array);

        System.out.println("Minimum: " + array[0] + " ekstremum: " + array[array.length-1]);
    }
}
