package pl.klasy2Zadanie4;

public class Employee {
    private String firstName;
    private String secondName;
    private int yearOfBirth;
    private int experience;
    private double sal;

    private static double salSum=0;

    public Employee(String firstName, String secondName, int yearOfBirth, int experience, double sal) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.yearOfBirth = yearOfBirth;
        this.experience = experience;
        this.sal=sal;
        salSum+=sal;
    }

    public static double getSalarySum() {
        return salSum;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                ", experience=" + experience +
                '}';
    }
}
