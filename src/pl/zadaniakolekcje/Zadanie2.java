package pl.zadaniakolekcje;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Zadanie2 {

    static Map howManyTimesNumbersAreRepeated(int[] A) {
        Arrays.sort(A);

        Map<Integer, Integer> mapOfRepeatings = new HashMap<>();
        int counter=1;
        int numberToCompareWith=A[0];
        for(int i=1; i<A.length; i++) {
            if(A[i]==numberToCompareWith) counter++;
            else {
                mapOfRepeatings.put(numberToCompareWith, counter);
                numberToCompareWith=A[i];
                counter=1;
            }
        }

        mapOfRepeatings.put(numberToCompareWith, counter);

        return mapOfRepeatings;
    }

    public static void main(String[] args) {
        int[] array={1,2,4,4,4,3,3,-1,-1,-1,2,6,6,6,7,6,5,6,7,6,5,6,7};
        Map<Integer, Integer> newMap=howManyTimesNumbersAreRepeated(array);
        for(Map.Entry<Integer, Integer> entry : newMap.entrySet()) {
            System.out.println(entry.getKey()+ " występuje " + entry.getValue() + " razy." );
        }
    }
}
