package pl.zadaniakolekcje;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zadanie3 {

    static final Map<String,String> dictionary =new HashMap<>();

    static {
        dictionary.put("mama", "mother");
        dictionary.put("tata", "father");
        dictionary.put("długopis", "pen");
    }
    static String translate(String word) {
        return dictionary.get(word.toLowerCase());
    }

    public static void main(String[] args) {
        System.out.println("Podaj wyraz po Polsku: ");
        Scanner sc= new Scanner(System.in);
        String word=sc.nextLine();
        System.out.println("Wyraz po angielsku to:      " + translate(word));
    }


}
