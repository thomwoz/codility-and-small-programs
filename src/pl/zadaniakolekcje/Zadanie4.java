package pl.zadaniakolekcje;

import java.util.*;

public class Zadanie4 {

    static List getBinaryNumber(int x) {
        List<Integer> list = new ArrayList<>();
        int oneToShiftBinary=1;
        while(oneToShiftBinary!=0 && x!=0) {
            if ((oneToShiftBinary & x) != 0) {
                list.add(1);
                x=x^oneToShiftBinary;

            } else {
                list.add(0);
            }
            oneToShiftBinary <<= 1;
        }

        Collections.reverse(list);
        return list;
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbę: ");

        Scanner sc=new Scanner(System.in);
        int number=sc.nextInt();

        List<Integer> list=getBinaryNumber(number);

        System.out.println("Postać liczby binarnej: ");
        if(list.size()<4) System.out.print("000");
        for(Integer oneOrZero : list) {
            System.out.print(oneOrZero);
        }
    }

}
