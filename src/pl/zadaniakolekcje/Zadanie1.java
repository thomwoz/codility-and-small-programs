package pl.zadaniakolekcje;



import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Zadanie1 {
    static void duzyLotek() {
        Set<Integer> set=new TreeSet<>();
        Random random= new Random();
        int r=random.nextInt(49)+1;
        for(int i=0; i<6; i++) {
            while(set.contains(r)) {
                r=random.nextInt(49)+1;
            }
            set.add(r);
        }
        System.out.println("Wylosowane liczby to: ");
        for (int number: set) {
            System.out.println(number);
        }
    }

    public static void main(String[] args) {
        duzyLotek();

    }
}
