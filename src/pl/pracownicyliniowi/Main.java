package pl.pracownicyliniowi;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static List<Boss> bossList=new ArrayList<>();
    public static void main(String[] args) {

        Main.bossList.add(new Boss("Jan", "Kowalski"));
        Main.bossList.get(0).addEmployee("Marek", "Piskorz");
        Main.bossList.get(0).getEmployeeList().get(0).addRegistry(7, "Mycie naczyń + jedzenie");
        Main.bossList.get(0).addEmployee("Darek", "Piskorz");
        Main.bossList.get(0).getEmployeeList().get(1).addRegistry(7, "Sprzątanie");

        Main.bossList.add(new Boss("Andrzej", "Kowalski-Dyrektor"));
        Main.bossList.get(1).addEmployee("Marek", "Kolarski");
        Main.bossList.get(1).getEmployeeList().get(0).addRegistry(3, "Zmywanie podłogi");
        Main.bossList.get(1).getEmployeeList().get(0).addRegistry(3, "Mycie okien.");

        for(int i=0; i<Main.bossList.size(); i++) {
            System.out.println("**********************");
            Main.bossList.get(i).showEBossAndEmployees();
            System.out.println("**********************");
        }

        System.out.println();
        for(int i=0; i<Main.bossList.size(); i++) {
            System.out.println(Main.bossList.get(i));
            Main.bossList.get(i).showAllRegistryForWorkers();
        }
    }
}
