package pl.pracownicyliniowi;

import java.util.ArrayList;
import java.util.List;

public class LineEmployee {
    private List<Registry> registryList;
    private static List<LineEmployee> allEmployees=new ArrayList<>();

    public static List<LineEmployee> getAllEmployees() {
        return allEmployees;
    }

    String name, surname;

    public LineEmployee() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LineEmployee(String name, String surname) {
        this.name = name;
        this.surname = surname;
        registryList=new ArrayList<>();
        allEmployees.add(this);
    }

    public void showRegistries(){
        for(Registry r:registryList){
            System.out.println(r);
        }
    }

    public void addRegistry(int workingHours, String tasksOfTheDay){
        registryList.add(new Registry(workingHours,tasksOfTheDay, this));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name + " " + surname);
        for (Registry r : registryList) {
            sb.append("\n                      " +r);
        }
        return sb.toString();
    }

}

