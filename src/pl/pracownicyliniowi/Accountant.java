package pl.pracownicyliniowi;

import java.util.ArrayList;

public class Accountant extends LineEmployee {
    public Accountant(String name, String surname) {
        super(name,surname);
        LineEmployee.getAllEmployees().add(this);
    }
}
