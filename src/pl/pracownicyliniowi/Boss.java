package pl.pracownicyliniowi;

import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.List;

public class Boss extends LineEmployee{
    private List<LineEmployee> employeeList;

    public List<LineEmployee> getEmployeeList() {
        return employeeList;
    }

    public Boss(String name, String surname) {
        super(name,surname);
        this.employeeList = new ArrayList<>();
        LineEmployee.getAllEmployees().add(this);
    }

    public void addEmployee(String name, String surname) {
        employeeList.add(new LineEmployee(name,surname));
    }

    public void showEBossAndEmployees(){
        System.out.println(this.getName()+" "+this.getSurname());
        for(LineEmployee emp:employeeList){
            System.out.println("        " + emp);
        }
    }

    public void showAllRegistryForWorkers() {
        for(LineEmployee emp:employeeList) {
            emp.showRegistries();
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
