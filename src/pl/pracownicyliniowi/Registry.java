package pl.pracownicyliniowi;

public class Registry {
    private static int counter=0;
    private int id;
    int workingHours;
    String tasksOfTheDay;
    LineEmployee employee;

    public Registry(int workingHours, String tasksOfTheDay, LineEmployee employee) {
        this.workingHours = workingHours;
        this.tasksOfTheDay = tasksOfTheDay;
        this.employee = employee;
        counter++;
        id=counter;
    }

    public int getId() {
        return id;
    }

    public int getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(int workingHours) {
        this.workingHours = workingHours;
    }

    public String getTasksOfTheDay() {
        return tasksOfTheDay;
    }

    public void setTasksOfTheDay(String tasksOfTheDay) {
        this.tasksOfTheDay = tasksOfTheDay;
    }

    public LineEmployee getEmployee() {
        return employee;
    }

    public void setEmployee(LineEmployee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "id=" + id+ ", workingHours=" + workingHours +
                ", tasksOfTheDay='" + tasksOfTheDay + '\'' +
                ", employee=" + employee.getName()+" "+employee.getSurname();
    }
}
