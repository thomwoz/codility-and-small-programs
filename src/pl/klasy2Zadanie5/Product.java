package pl.klasy2Zadanie5;

public class Product {

    private String name;
    private String description;
    private String specification;

    public Product(String name, String description, String specification) {
        this.name = name;
        this.description = description;
        this.specification = specification;
    }
}
