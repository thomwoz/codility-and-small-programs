package pl.klasy2Zadanie5;
import java.time.LocalDate;

public class Guarantee {
    private Product product;
    private LocalDate validUntil;


    public Guarantee(Product product, LocalDate validUntil) {
        this.product = product;
        this.validUntil = validUntil;
    }


    public void isValid(){
        if (!LocalDate.now().isBefore(validUntil)) {
            product=null;
            validUntil=null;
        }
    }

    public static void main(String[] args) {
        Product product= new Product("Pralka", "Urządzenie do prania. ", "Wymiary: 100x56");
        Guarantee g=new Guarantee(product, LocalDate.of(2018,01,13));
        Guarantee g2=new Guarantee(product, LocalDate.of(2017,01,13));

    }

}
