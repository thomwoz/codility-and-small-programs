package pl.homework1;

public class Lightbulb {

    private int lifespan;
    private int power;

    public Lightbulb(){
    }

    public Lightbulb(int lifespan, int power) {
        this.lifespan = lifespan;
        this.power = power;
    }

    public void showLifespanAndPower() {
        if(lifespan+power==0) System.out.println("Brak danych.");
        else System.out.println("Life span: " + lifespan + " months |  Power: " + power + "W");
    }

    public int getLifespan() {
        return lifespan;
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
