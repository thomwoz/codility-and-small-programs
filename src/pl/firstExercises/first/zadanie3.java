package pl.firstExercises.first;

import java.util.InputMismatchException;
import java.util.Scanner;

public class zadanie3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a=0;
        System.out.println("Wprowadź swój wiek: \n");
        try {
            a=scanner.nextInt();

        } catch(InputMismatchException e) {
            System.out.println("Podana wartość musi być liczbą!");
        }


            if (a < 1 || a > 150) {
                System.out.println("Imponujący wiek.. ktoś chyba oszukuje.");
            } else {
                if (a < 18) System.out.println("Jesteś niepełnoletni!");
                else System.out.println("Osoba pełnoletnia.");
            }

    }
}
