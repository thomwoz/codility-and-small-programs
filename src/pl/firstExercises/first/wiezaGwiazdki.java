package pl.firstExercises.first;

public class wiezaGwiazdki {
    public static void main(String[] args) {
        int height = 5;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < (height * 2) - 1; j++) {
                if (j >= (height - 1) - i && j <= (height - 1) + i)
                    System.out.print("*");
                else System.out.print("-");
            }
            System.out.println("");
        }

        System.out.println("");

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < (height * 2); j++) {
                if (j >= (height - 1) - i && j <= (height) + i)
                    System.out.print("*");
                else System.out.print("-");
            }
            System.out.println("");
        }

    }
}

