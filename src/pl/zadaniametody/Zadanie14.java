package pl.zadaniametody;

public class Zadanie14 {
    static int NWD(int a, int b) {
        int tmp;
        if(b>a) {
            a=a+b;
            b=a-b;
            a=a-b;
        }
        while(b!=0) {
            tmp=a%b;
            a=b;
            b=tmp;
        }
        return a;
    }

    static int NWD2(int a, int b) {
        while(a!=b) {
            if(a>b) a=a-b;
            else b=b-a;
        }
        return a;
    }
    public static void main(String[] args) {
//        System.out.println("NWD: " +NWD(Zadanie1.numberTypedByUser(),Zadanie1.numberTypedByUser()));
        System.out.println("NWD: " +NWD(Zadanie1.numberTypedByUser(),Zadanie1.numberTypedByUser()));
    }
}
