package pl.zadaniametody;

public class Zadanie4 {
    static int divide(int a, int b) {
        if (b == 0) {

            System.out.println("Nie można dzielić przez 0. ");
            return 0;
        } else return a / b;

    }

    public static void main(String[] args) {
        int a = Zadanie1.numberTypedByUser();
        int b = Zadanie1.numberTypedByUser();

        System.out.println("Wynik dzielenia: " + divide(a, b));
    }
}
