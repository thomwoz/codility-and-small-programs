package pl.zadaniametody;

import java.util.Scanner;

public class Zadanie1 {

    static int add(int a, int b) {
        return a+b;
    }

    static int numberTypedByUser(){
        System.out.println("Podaj liczbę: ");
        Scanner scanner= new Scanner(System.in);
        return scanner.nextInt();
    }

    public static void main(String[] args) {
        int a=numberTypedByUser();
        int b=numberTypedByUser();

        System.out.println("Suma= " + add(a,b));
    }
}
