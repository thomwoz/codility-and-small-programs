package pl.zadaniametody;

public class Zadanie5 {

    static boolean arePitagorasNumbers(int a, int b, int c) {
        int a2=a*a;
        int b2=b*b;
        int c2=c*c;
        if(a+b==c || a+c==b || c+b==a) return true;
        else return false;
    }

    public static void main(String[] args) {
        int a = Zadanie1.numberTypedByUser();
        int b = Zadanie1.numberTypedByUser();
        int c = Zadanie1.numberTypedByUser();

        if(arePitagorasNumbers(a,b,c)) System.out.println("Liczby są pitagorejskie");
        else System.out.println("Liczby nie są pitagorejskie");
    }
}
