package pl.zadaniametody;

public class Zadanie6 {
    static int sumOfNumbers(int a) {
        int sum = 0;
        System.out.println(a / 10);
        do {
            sum += (a % 10);
            a = a / 10;

        } while (a!=0);
        return sum;
    }

    static boolean isSquareOfNumber(int a) {
        int sqrt=(int)Math.sqrt(a);
        return sqrt*sqrt==a ? true : false;
        }

    static boolean isCube(int a) {
        int cube=(int)Math.pow(a, 1.0/3);
        return cube*cube*cube==a ? true : false;
    }

     static boolean isPrime(int n) {
        return numberOfDivisors(n)==2 ? true : false;
    }

    static int numberOfDivisors(int n) {
        int nrOfDiv=0;
        for(int i=1; i<=n; i++) {
            if(n%i==0) nrOfDiv++;
        }

        return nrOfDiv;
    }

    int numberOfDivisors2(int n) {
        int nrOfDiv=2;
        for(int i=1; i<Math.sqrt(n); i++) {
            if(n%i==0) nrOfDiv+=2;
        }

        if(isSquareOfNumber(n)) nrOfDiv--;
        return nrOfDiv;
    }



    static double power(double n, int a) {
        return Math.pow(n,a);
    }



    public static void main(String[] args) {
//      System.out.println(sumOfNumbers(Zadanie1.numberTypedByUser()));
//      System.out.println(isSquareOfNumber(Zadanie1.numberTypedByUser()));


        //zadanie12
        int currentMaxNumberOfDivisors;
        int maxNrOfDivisors=numberOfDivisors(2);
        int maxNumber=2;
        for(int i=2+1; i<=10000; i++) {
            currentMaxNumberOfDivisors=numberOfDivisors(i);
            if(currentMaxNumberOfDivisors>maxNrOfDivisors) {
                maxNrOfDivisors=currentMaxNumberOfDivisors;
                maxNumber=i;
            }
        }
        System.out.println("liczba  "+ maxNumber + " dzielników: "+maxNrOfDivisors);
    }
}
