package pl.zadaniametody;

public class Zadanie2 {

    static int substract(int a, int b) {
        return a-b;
    }

    public static void main(String[] args) {
        int a=Zadanie1.numberTypedByUser();
        int b=Zadanie1.numberTypedByUser();

        System.out.println("Wynik odejmowania a-b: " + substract(a,b));
    }
}
