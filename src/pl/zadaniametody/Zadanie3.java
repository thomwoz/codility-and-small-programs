package pl.zadaniametody;

public class Zadanie3 {
    static int multiply(int a, int b){
        return a*b;
    }

    public static void main(String[] args) {
        int a=Zadanie1.numberTypedByUser();
        int b=Zadanie1.numberTypedByUser();

        System.out.println("Wynik mnożenia a i b: " + multiply(a,b));
    }
}
