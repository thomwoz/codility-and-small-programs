package pl.kolekcje;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class listEx {

    private static boolean areThereMoreThanOneElement(List<String> list){
        int i=0;
        while(i<list.size()){
            String element = list.get(i);
            int z=i+1;
            while(z<list.size()){
                if(element.equals(list.get(z))) return true;
                else z++;
            }
            i++;
        }
        return false;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Asia");
        list.add("Basia");
        list.add("Wojtek");
        list.add("Krzysiek");
        list.add("Wojtek1");

        if(areThereMoreThanOneElement(list)) {
            System.out.println("W liscie powtarzają się elementy.");
        }
        else {
            System.out.println("Nie powtarzają się!");
        }
    }
}
