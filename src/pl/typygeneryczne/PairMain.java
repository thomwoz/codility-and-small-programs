package pl.typygeneryczne;

public class PairMain {
    public static void main(String[] args) {
        Pair<String, Integer> pg = new Pair<>("Jan", 3);
        System.out.println(pg.getFirst() + " " + pg.getLast());
        String name = pg.getFirst();
        int m = pg.getLast();
        pg.setFirst(name + " Kowalski");
        pg.setLast(m+1);
        System.out.println(pg.getFirst() + " " + pg.getLast());


        Pair<Integer, Integer> position=new Pair<>(13,2);

        Pair<String, String> osoba=new Pair<>("Jan", "Kowalski");

        Pair<Integer, String> idOsoby=new Pair<>(1,"Dawid");

        PairTwo<String, String> pairTwo=new PairTwo<>("Jan","Kowalski");

        System.out.println(pairTwo);
    }
}
