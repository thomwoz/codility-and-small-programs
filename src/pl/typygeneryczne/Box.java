package pl.typygeneryczne;


import java.util.ArrayList;
import java.util.List;

public class Box<T> {
    List<T> list = new ArrayList<>();

    public void addElement(T e){
        list.add(e);
    }

    public void removeElement(T e){
        list.remove(e);
    }

}
