package pl.typygeneryczne;

public class Pair <T, T2>{
    private T first;
    private T2 last;

    public Pair(T first, T2 last) {
        this.first = first;
        this.last = last;
    }

    public Pair(){}

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T2 getLast() {
        return last;
    }

    public void setLast(T2 last) {
        this.last = last;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", last=" + last +
                '}';
    }
}
