package pl.typygeneryczne;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ArrayToCollection<T> {
    public List<T> arrayToCollection(T[] array){
        List<T> collection=new ArrayList<T>();
        for(T element:array) {
            collection.add(element);
        }
        return collection;
    }

    public void arrayToCollectionTwo(T[] array,Collection<T> collection){
            collection.addAll(Arrays.asList(array));
    }

    public void showList(List<T> list){
        for(T p:list){
            System.out.println(p);
        }
    }
    public static void main(String[] args) {

        ArrayToCollection<String> collection=new ArrayToCollection<>();
        String[] array={"Jan", "das", "dasdas","dsa","dsadas"};

        collection.showList(collection.arrayToCollection(array));


        System.out.println();

        ArrayToCollection<Integer> collection2=new ArrayToCollection<>();
        Integer[] array2={1,2,3,4,5,6,7,8,9};

        collection2.showList(collection2.arrayToCollection(array2));
    }
}
