package pl.klasyzadaniaprezentacja;

import java.util.Comparator;


public class Book implements Comparator<Book>, Comparable<Book>  {
    private String title;
    private Integer nrOfPages;
    private Double price;

    public Book(String title, int nrOfPages, double price) {
        this.title = title;
        this.nrOfPages = nrOfPages;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNrOfPages() {
        return nrOfPages;
    }

    public void setNrOfPages(int nrOfPages) {
        this.nrOfPages = nrOfPages;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int compareTo(Book o) {
        return (this.price).compareTo(o.getPrice());
    }

    public Book(){

    }

    @Override
    public int compare(Book o1, Book o2) {
        return o1.nrOfPages - o2.nrOfPages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", nrOfPages=" + nrOfPages +
                ", price=" + price +
                '}';
    }
}
