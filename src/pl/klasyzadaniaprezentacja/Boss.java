package pl.klasyzadaniaprezentacja;

import java.util.ArrayList;
import java.util.List;

public class Boss extends Employee {
    public static List<Boss> sumOfBoss=new ArrayList<>();
    private List<Employee> employeeList=new ArrayList<>();

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public Boss(String name, String surname) {
        super(name, surname);
        sumOfBoss.add(this);
    }

    public Boss() {

    }

    public void addEmployee(Employee emp) {
        employeeList.add(emp);
        Employee.employees.add(emp);
    }

    @Override
    public String toString() {
        return "Kierownik: "+this.getName() + " "+this.getSurname();
    }
}
