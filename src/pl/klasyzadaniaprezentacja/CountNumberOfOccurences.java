package pl.klasyzadaniaprezentacja;

import java.util.*;

public class CountNumberOfOccurences {

    public static Map<String, Integer> getNrOfOccurences(List<String> list){
        Collections.sort(list);
        Map<String, Integer> map=new HashMap<>();

        int nrOfOccurrences=1;
        String objectToCompareWith=list.get(0);

        for(int i=1; i< list.size(); i++) {
            System.out.println();
            System.out.println("Iteracja nr "+ i);
            if(objectToCompareWith.equals(list.get(i))) {
                System.out.println("list["+i+"]="+list.get(i));
                nrOfOccurrences++;
                System.out.println("nrofOcurrences: "+nrOfOccurrences);
            }
            else {
                map.put(objectToCompareWith, nrOfOccurrences);
                System.out.println("objecttocompare="+objectToCompareWith);
                System.out.println("nrofOcc="+nrOfOccurrences);
                objectToCompareWith=list.get(i);
                System.out.println("ObjecttoCompare po zmianie="+objectToCompareWith);
                nrOfOccurrences=1;
            }
        }
        map.put(objectToCompareWith,nrOfOccurrences);

        return map;
    }



    public static void main(String[] args) {
        List<String> list=new ArrayList<>();

        list.add("dup");
        list.add("dup");
        list.add("aaa");
        list.add("dup");
        list.add("vnb");
        list.add("vnb");
        list.add("aaa");


        Map<String, Integer> map=getNrOfOccurences(list);
        for(String word:map.keySet()) {
            System.out.println("Słowo: " + word + " ilość powtórzeń: " + map.get(word));
        }
    }
}
