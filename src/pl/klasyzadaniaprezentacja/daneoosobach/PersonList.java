package pl.klasyzadaniaprezentacja.daneoosobach;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonList {
    private List<Person> personList;

    public PersonList() {
        this.personList = new ArrayList<>();
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void addPerson() {
        Scanner sc=new Scanner(System.in);
        System.out.println("Imię: ");
        String name=sc.nextLine();
        System.out.println("Imię: ");
        String surname=sc.nextLine();
        System.out.println("Imię: ");
        int age=sc.nextInt();
        System.out.println("Imię: ");
        int height=sc.nextInt();
        System.out.println("Imię: ");
        double weight=sc.nextDouble();
        personList.add(new Person(name,surname,age,height,weight));
    }

    public List<Person> searchBySurname(String surname) {
        List<Person> person=new ArrayList<>();
        for(Person p:getPersonList())
            if(surname.equals(p.getName())) person.add(p);
        if(person.isEmpty())System.out.println("Brak takiej osoby");
        return person;
    }
    public List<Person> searchByName(String name) {
        List<Person> person=new ArrayList<>();
        for(Person p:getPersonList())
            if(name.equals(p.getName())) person.add(p);
        if(person.isEmpty())System.out.println("Brak takiej osoby");
        return person;
    }

    public List<Person> searchByAge(Integer age) {
        List<Person> person=new ArrayList<>();
        for(Person p:getPersonList())
            if(age==p.getAge()) person.add(p);
        if(person.isEmpty())System.out.println("Brak takiej osoby");
        return person;
    }

    public List<Person> searchByHeight(Integer height) {
        List<Person> person=new ArrayList<>();
        for(Person p:getPersonList())
            if(height==p.getHeight()) person.add(p);
        if(person.isEmpty())System.out.println("Brak takiej osoby");
        return person;
    }

    public List<Person> searchByWeight(Double weight) {
        List<Person> person=new ArrayList<>();
        for(Person p:getPersonList())
            if(weight==p.getWeight()) person.add(p);
        if(person.isEmpty())System.out.println("Brak takiej osoby");
        return person;
    }

    public Person findOldestPerson(){
        Person oldestPerson=personList.get(0);
        for(Person p:personList) if(p.getAge()>oldestPerson.getAge()) oldestPerson=p;

        return oldestPerson;
    }

    public Person findYoungestPerson(){
        Person youngestPerson=personList.get(0);
        for(Person p:personList) if(p.getAge()<youngestPerson.getAge()) youngestPerson=p;

        return youngestPerson;
    }

    public Person findHighestPerson(){
        Person highestPerson=personList.get(0);
        for(Person p:personList) if(p.getHeight()>highestPerson.getHeight()) highestPerson=p;

        return highestPerson;
    }

    public Person findShortestPerson(){
        Person shortestPerson=personList.get(0);
        for(Person p:personList) if(p.getHeight()<shortestPerson.getHeight()) shortestPerson=p;

        return shortestPerson;
    }

    public Person findFattest(){
        Person fatPerson=personList.get(0);
        for(Person p:personList) if(p.getWeight()>fatPerson.getWeight()) fatPerson=p;

        return fatPerson;
    }

    public Person findSkinny(){
        Person skinnyPerson=personList.get(0);
        for(Person p:personList) if(p.getWeight()<skinnyPerson.getWeight()) skinnyPerson=p;
        return skinnyPerson;
    }


}
