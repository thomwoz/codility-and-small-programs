package pl.klasyzadaniaprezentacja;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BookMain {
    public static void main(String[] args) {
        List<Book> bookList=new ArrayList<>();
        bookList.add(new Book("WWW", 15,343));
        bookList.add(new Book("rdsfds", 27,23));
        bookList.add(new Book("dfsada", 77,453));
        bookList.add(new Book("dsafds", 55,345));
        bookList.add(new Book("vbcxbvxcbvcx", 25,765));
        bookList.add(new Book("bvcxbvxc", 99,4));
        bookList.add(new Book("jhgjhg", 54,34));
        bookList.add(new Book("uitutoiy", 13,65));

        Collections.sort(bookList);
        for(Book book: bookList){
            System.out.println(book);
        }

        System.out.println(
        );

        System.out.println();


        Collections.sort(bookList, new Book());
        for(Book book: bookList){
            System.out.println(book);
        }


    }
}
