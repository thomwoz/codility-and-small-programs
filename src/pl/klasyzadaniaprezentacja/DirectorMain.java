package pl.klasyzadaniaprezentacja;

import java.util.ArrayList;
import java.util.List;

public class DirectorMain {
    public static void main(String[] args) {
        List<Director> directorList=new ArrayList<>();
        //dodajemy dyrektorów
        directorList.add(new Director("Janusz", "Kowalski"));
        directorList.add(new Director("Marek", "Kowalski"));
        directorList.add(new Director("Andrzej", "Kowalski"));
        //dodajemy podwladnych kierowników
        directorList.get(0).addBoss(new Boss("Arek", "Marek"));
        directorList.get(0).addBoss(new Boss("Arek", "Darek"));

        directorList.get(1).addBoss(new Boss("Arek", "Marek"));
        directorList.get(1).addBoss(new Boss("Arek", "Darek"));

        directorList.get(2).addBoss(new Boss("Arek", "Marek"));
        directorList.get(2).addBoss(new Boss("Arek", "Darek"));

        //dodajemy podwładnych (kierownikom) pracowników

        directorList.get(0).getBossList().get(0).addEmployee(new Employee("Konrad", "Gustaw"));
        directorList.get(0).getBossList().get(0).addEmployee(new Employee("Konrad", "Justaw"));

        directorList.get(0).getBossList().get(1).addEmployee(new Employee("Konrad", "Gustaw"));
        directorList.get(0).getBossList().get(1).addEmployee(new Employee("Konrad", "Gustaw"));

        directorList.get(1).getBossList().get(0).addEmployee(new Employee("Konrad", "Gustaw"));
        directorList.get(1).getBossList().get(0).addEmployee(new Employee("Konrad", "Justaw"));

        directorList.get(1).getBossList().get(1).addEmployee(new Employee("Konrad", "Gustaw"));
        directorList.get(1).getBossList().get(1).addEmployee(new Employee("Konrad", "Gustaw"));

        directorList.get(2).getBossList().get(0).addEmployee(new Employee("Konrad", "Gustaw"));
        directorList.get(2).getBossList().get(0).addEmployee(new Employee("Konrad", "Justaw"));

        directorList.get(2).getBossList().get(1).addEmployee(new Employee("Konrad", "Gustaw"));
        directorList.get(2).getBossList().get(1).addEmployee(new Employee("Konrad", "Gustaw"));

        for(Director director:directorList){
            System.out.println(director);
            for(Boss boss:director.getBossList()) {
                System.out.println("               " + boss);
                for(Employee emp:boss.getEmployeeList()) {
                    System.out.println("                              "+emp);
                }
            }
        }

        System.out.println();

        for(Boss boss:Boss.sumOfBoss) {
            System.out.println(boss);
       }

        System.out.println();

        for(Employee emp:Employee.employees) {
            System.out.println(emp);
        }
    }
}
