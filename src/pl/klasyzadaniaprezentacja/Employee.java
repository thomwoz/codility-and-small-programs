package pl.klasyzadaniaprezentacja;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    public static List<Employee> employees = new ArrayList<>();
    private String name,surname;

    public String getName() {
        return name;
    }

    public Employee(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Employee() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Pracownik: " +name + " " + surname;
    }
}
