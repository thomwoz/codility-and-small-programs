package pl.klasyzadaniaprezentacja;

import java.util.ArrayList;
import java.util.List;

public class Director extends Boss{
    private List<Boss> bossList=new ArrayList<>();

    public List<Boss> getBossList() {
        return bossList;
    }

    public Director(String name, String surname) {
        this.setName(name);
        this.setSurname(surname);
    }

    public void addBoss(Boss boss){
        bossList.add(boss);
    }

    @Override
    public String toString() {
        return "Dyrektor: "+this.getName() + " "+this.getSurname();
    }
}
