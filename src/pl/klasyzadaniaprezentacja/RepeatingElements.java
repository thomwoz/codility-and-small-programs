package pl.klasyzadaniaprezentacja;

import java.util.*;

public class RepeatingElements {

    public static boolean hasRepeatingElements(List<String> list) {
        Collections.sort(list);
        for(int i=0; i< list.size()-1; i++) {
            if(list.get(i).equals(list.get(i+1))) return true;
        }
        return false;
    }

    public static boolean hasRepeatingElements2(List<String> list) {
        Set<String> set=new HashSet<>();
        for(int i=0; i< list.size(); i++) {
            if(set.contains(list.get(i))) return true;
            set.add(list.get(i));
        }
        return false;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("WWW");
        list.add("zWWW");
        list.add("xWWW");
        list.add("fWWW");
        list.add("yWWW");
        list.add("rWWW");
        list.add("www");
        list.add("WWW");

        System.out.println("Rozwiązanie 1. ");

        if(hasRepeatingElements(list)) System.out.println("Elementy powtarzają się. ");
        else System.out.println("Nie powtarzają się. ");

        System.out.println("Rozwiązanie 2. ");
        if(hasRepeatingElements2(list)) System.out.println("Elementy powtarzają się. ");
        else System.out.println("Nie powtarzają się. ");

    }
}
