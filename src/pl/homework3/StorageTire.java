package pl.homework3;

import java.util.HashMap;

public class StorageTire {

    HashMap<String, Tire> myMap = new HashMap<String, Tire>();

    public void addTire(Tire tire, int z, int x, int y) {

        if (myMap.get(x + "/" + y + "/" + z) == null) {
            myMap.put(x + "/" + y + "/" + z, tire);
            System.out.println("Dodałem opone!");
        } else {
            System.out.println("Miejsce zajete!");
        }
    }


    public Tire getTire(int x, int y, int z) {
        return myMap.get(x + "/" + y + "/" + z);
    }
}
