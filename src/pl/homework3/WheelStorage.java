package pl.homework3;

import java.util.ArrayList;
import java.util.List;

public class WheelStorage {
    private Wheel wheel;
    private int x,y,z;



    public WheelStorage(Wheel wheel, int x, int y, int z) {
        this.wheel = wheel;
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
