package pl.homework3;

public class Wheel {

    private int width;
    private int profile;
    private double radius;


    public Wheel(int width, int profile, double radius) {
        this.width = width;
        this.profile = profile;
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "["+width+"/"+profile+" R"+radius+"]";
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
