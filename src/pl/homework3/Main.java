package pl.homework3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static List<WheelStorage> wheelStorageList;

    public void addWheel(Wheel wheel, int x, int y, int z) {
        for (WheelStorage ws : wheelStorageList) {
            if ((ws.getX() == x) && (ws.getY() == y) && (ws.getZ() == z)) {
                System.out.println("Miejsce zajęte. ");
                break;
            } else wheelStorageList.add(new WheelStorage(wheel, x, y, z));
        }
    }

    public Wheel getWheelFromStorage(int x, int y, int z) {
        Wheel wheel=new Wheel(170,70,54);
        for (WheelStorage ws : wheelStorageList) {
            if (ws.getX() == x && ws.getY() == y && ws.getZ() == z) wheel=ws.getWheel();
        }
        return wheel;
    }

    public static void main(String[] args) {
        wheelStorageList=new ArrayList<>();
        wheelStorageList.add(new WheelStorage(new Wheel(14,14,14),1,2,3));

    }


}
