package pl.zadaniapowtorkowe;

import java.util.Scanner;

public class Zadanie6 {

    static boolean isDividedby3(int n) {
        if(n%3==0) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);

        System.out.println("Podaj liczbę ");

        int a=scanner.nextInt();
        if(isDividedby3(a)) System.out.println("Podzielna przez 3.");
        else System.out.println("Niepodzielna przez 3.");


    }

}
