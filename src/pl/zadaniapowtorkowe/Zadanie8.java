package pl.zadaniapowtorkowe;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj 1 wyraz: ");
        String wordA=scanner.nextLine();
        System.out.println("Podaj 2 wyraz: ");
        String wordB=scanner.nextLine();

        if(wordA.equals(wordB)) System.out.println("Wyrazy takie same.");
        else System.out.println("Wcale nie takie same.");
    }
}
