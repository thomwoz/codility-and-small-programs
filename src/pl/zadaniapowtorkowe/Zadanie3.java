package pl.zadaniapowtorkowe;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wyraz: ");
        String word=scanner.nextLine();
        System.out.println("Przedostatnia litera wyrazu to: " + word.charAt(word.length()-2));
    }
}
