package pl.zadaniapowtorkowe;

import java.util.Scanner;

public class Zadanie4True {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);

        System.out.println("Podaj ciąg znaków: ");

        String charSequence=scanner.nextLine();

        System.out.println("Cyfry z ciągy znaków: ");
        for(int i=0; i<charSequence.length(); i++) {
            if(Character.isDigit(charSequence.charAt(i))) System.out.print(charSequence.charAt(i) + " ");
        }
    }
}
