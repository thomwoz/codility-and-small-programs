package pl.zadaniapowtorkowe;

import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        int[] array=new int[3];
        Scanner scanner=new Scanner(System.in);
        int sum=0;
        for(int i=0; i<array.length; i++){
            System.out.println("Wprowadź liczbę: ");
            array[i]=scanner.nextInt();
            sum+=array[i];
        }

        System.out.println("średnia tych liczb wynosi: " + (double)sum/3.0);
    }
}
