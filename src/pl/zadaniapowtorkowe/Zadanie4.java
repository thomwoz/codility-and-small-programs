package pl.zadaniapowtorkowe;

import java.util.Scanner;

public class Zadanie4 {

    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);

        System.out.println("Podaj kod pocztowy: ");

        String postalNumber=scanner.nextLine();
        boolean isPostalNumber=true;

            if(postalNumber.length()>6 || postalNumber.charAt(2)!='-') isPostalNumber=false;
            else {
                for(int i=0; i<6;i++) {
                    if(i==2) continue;
                    if(!Character.isDigit(postalNumber.charAt(i))) { isPostalNumber=false; break; }
                }
            }
        if(isPostalNumber) System.out.println("Poprawny kod pocztowy.");
            else System.out.println("Niepoprawny kod pocztowy.");

    }
}
