package pl.zadaniaPodstawowe;

        import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj ilość rzędów: ");
        int nrOfRows=scanner.nextInt();
        System.out.println("Podaj ilość miejsc: ");
        int nrOfSeats=scanner.nextInt();

        if(nrOfSeats%nrOfRows==0) System.out.println("Ilość miejsc w rzędzie: " + nrOfSeats/nrOfRows);
        else  System.out.println("Ilość miejsc w rzędzie: " + nrOfSeats/(nrOfRows-1) + "\n" +
                "Ostatni rząd nieregularny, ilość miejsc:" + nrOfSeats%(nrOfRows-1));
    }
}
