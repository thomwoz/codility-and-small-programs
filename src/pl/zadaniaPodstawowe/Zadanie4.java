package pl.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a;
        int tmp;
        System.out.println("Podaj liczbę nr 1");
        tmp=scanner.nextInt();
        for(int i =0; i<3; i++){
            System.out.println("Podaj liczbę nr " + i+1);
            a=scanner.nextInt();
            if(a<tmp) tmp=a;
        }
        System.out.println("Najmniejsza liczba to: " + tmp);

    }
}
