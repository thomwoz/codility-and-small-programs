package pl.klasy2Zadanie3;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private String name;

    private List<Player> players;

    public Game(String name) {
        this.name = name;
        players=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void winner(){
        Player winner=new Player("","",0);
        for(Player p:players){
            if (p.getPoints()>100&&p.getPoints()>winner.getPoints()) winner=p;
        }
        if (winner.getPoints()==0) System.out.println("Brak zwycięzcy.");
        else System.out.println(winner.getName() + " username: " + winner.getUsername());
    }

    public void addPlayer(Player p ){
        players.add(p);
    }


}
