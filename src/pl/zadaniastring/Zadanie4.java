package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz: ");
        String word=scanner.nextLine();
        if(Character.isDigit(word.charAt(0))) System.out.println("jest to liczba");
        else System.out.println("Nie jest to liczba. ");
    }
}
