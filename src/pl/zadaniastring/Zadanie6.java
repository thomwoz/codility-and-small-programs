package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź imię: ");
        String name=scanner.nextLine();
        System.out.println("Wprowadź nazwisko: ");
        String surname=scanner.nextLine();
        System.out.println("Twoje imię i nazwisko to " + name + " " + surname);
    }
}
