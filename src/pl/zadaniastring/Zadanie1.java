package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Wprowadź wyraz: ");
        String word=scanner.nextLine();

        System.out.println("Długość wyrazu to: " + word.length());
    }
}
