package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz: ");
        String word=scanner.nextLine();
        if(word.charAt(0)==word.charAt(word.length()-1)) System.out.println("Ostani i pierwszy znak są takie same.");
        else System.out.println("Nie są równe.");
    }
}
