package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz 1: ");
        String word1=scanner.nextLine();
        System.out.println("Wprowadź wyraz 2: ");
        String word2=scanner.nextLine();

        if(word1.equals(word2)) System.out.println("Wyrazy są sobie równe: ");
        else System.out.println("Wyrazy nie są sobie równe.");
    }
}
