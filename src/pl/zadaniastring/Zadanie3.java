package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Wprowadź wyraz:");
        String word=scanner.nextLine();
        if(word.endsWith("M") || word.endsWith("m")) System.out.println("Tak.");
        else System.out.println("Nie.");
    }
}
