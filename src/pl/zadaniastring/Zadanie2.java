package pl.zadaniastring;

import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word=scanner.nextLine();
        int naturalNumber = scanner.nextInt();

        System.out.println(word.substring(word.length()-naturalNumber));
    }
}
