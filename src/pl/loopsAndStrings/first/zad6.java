package pl.loopsAndStrings.first;

import java.util.Scanner;

public class zad6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text = sc.nextLine();
        int lowerCaseLetterCount = 0;
        for (int i = 0; i < text.length(); i++) {
            if (Character.isLowerCase(text.charAt(i))) lowerCaseLetterCount++;
        }

        System.out.println("W danym napisie występuje " + lowerCaseLetterCount + " małych liter");
    }
}
