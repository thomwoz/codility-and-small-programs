package pl.metodytablice;

import java.util.HashSet;
import java.util.Set;

public class Zadanie1 {
    public int sum3(int[] nums) {
        int sum = 0;
        for (int number : nums) {
            sum += number;
        }
        Set<Integer> set=new HashSet<>();
        return sum;
    }


    public int[] makeLast(int[] nums) {
        int[] array = new int[2 * nums.length];
        array[array.length - 1] = nums[nums.length - 1];
        return array;
    }


    public int[] reverse3(int[] nums) {
        int[] array = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            array[array.length - 1 - i] = nums[i];
        }
        return array;
    }


    public int[] maxEnd3(int[] nums) {
        int greaterNum = nums[0] > nums[nums.length - 1] ? nums[0] : nums[nums.length - 1];
        int[] array = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            array[i] = greaterNum;
        }
        return array;
    }

    int sumLeft(int[] array, int i) {
        int sum=0;
        for(int j=0; j<i; j++) {
            sum+=array[j];
        }
        return sum;
    }

    int sumRight(int[] array, int i) {
        int sum=0;
        for(int j=i+1; j<array.length; j++) {
            sum+=array[j];
        }
        return sum;
    }

}
