package pl.produktykategorie;

public class Category {
    private String name;

    public String getCat() {
        return name;
    }

    public void setCat(String cat) {
        this.name = cat;
    }

    public Category(String cat) {
        this.name = cat;
    }


    @Override
    public String toString() {
        return name;
    }
}

