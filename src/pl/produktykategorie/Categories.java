package pl.produktykategorie;


import java.util.HashSet;

import java.util.Set;

public class Categories {
    private Set<Category> categories;

    public Set<Category> getCategories() {
        return categories;
    }


    public Categories() {
        this.categories = new HashSet<>();
    }


    public void addNewCategory(Category category) {

        getCategories().add(category);
    }

    public boolean isInCategoryList(Category cat) {
        for (Category z : getCategories()) {
            if (cat.getCat().equals(z.getCat())) return true;
        }
        return false;
    }

    public void deleteCategory(String s){
        Category cat=null;
        for(Category z: getCategories()) {
            if(z.getCat().equals(s)) cat=z;
        }
        if(cat!=null)
        categories.remove(cat);
        else System.out.println("Nie ma takiej kategorii. ");
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Category category : getCategories()) {
            s.append("- " + category + "\n");
        }
        return s.toString();
    }
}
