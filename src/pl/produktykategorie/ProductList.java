package pl.produktykategorie;

import java.util.ArrayList;
import java.util.List;

public class ProductList {
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    public ProductList() {
        this.productList = new ArrayList<>();
    }

    public void addProductToList(Product p){
        getProductList().add(p);
    }

    public void deleteProduct(String product) {
        Product p=null;
        for(Product z: getProductList()) {
            if(z.getName().equals(product)) p=z;
        }
        if(p!=null)
            productList.remove(p);
        else System.out.println("Nie ma takiego produktu. ");
    }

    @Override
    public String toString() {
        StringBuilder s= new StringBuilder();
        for(Product p: getProductList()) {
            s.append("Produkt: " + p.getName() + " - kat: " + p.getCategory());
        }

        return s.toString();
    }
}
