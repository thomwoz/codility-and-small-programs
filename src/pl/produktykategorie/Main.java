package pl.produktykategorie;

import java.util.Scanner;

public class Main {

    private static String inputData(){
        Scanner scanner=new Scanner(System.in);
        return scanner.nextLine();
    }

    public static void main(String[] args) {
        int programRunning;
        ProductList productList=new ProductList();
        Scanner sc=new Scanner(System.in);
        Categories categories=new Categories();
        Category simpleCat=new Category("");
        System.out.println("                                              >>>>>>>>> PRODUKTY I KATEGORIE <<<<<<<<<<<");
        do {
            System.out.println("1 - Dodaj kategorię 2 - Dodaj produkt " +
                    "3 - Wyświetl kategorie 4 - Wyświetl produkty " +
                    "5 - Usuń kategorię 6 - Usuń produkt 7 - Wyjdź z programu");
            programRunning=sc.nextInt();
            switch(programRunning){
                case 1: {
                    System.out.println("Dodaj nową kategorię: ");
                    String cat = inputData();
                    categories.addNewCategory(new Category(cat));
                    break;
                }
                case 2: {
                    if(categories.getCategories().isEmpty()) {
                        System.out.println("Brak kategorii. Dodaj najpierw kategorie.");
                        break;
                    }
                    System.out.println("Nazwa produktu: ");
                    String name=inputData();
                    System.out.println("Wpisz kategorię z listy: \n" + categories);
                    simpleCat.setCat(inputData());
                    while(!categories.isInCategoryList(simpleCat)) {
                        System.out.println("Brak kategorii. ");
                        System.out.println("Wpisz kategorię z listy: \n" + categories);
                        simpleCat.setCat(inputData());
                    }
                    productList.addProductToList(new Product(name,simpleCat));
                    break;
                }
                case 3: {
                    if(categories.getCategories().isEmpty()) System.out.println("Brak kategorii.");
                    System.out.println(categories);
                    break;
                }
                case 4: {
                    if(productList.getProductList().isEmpty()) System.out.println("Brak produktów. ");
                    System.out.println(productList);
                    break;
                }
                case 5: {
                    if(categories.getCategories().isEmpty()) {
                        System.out.println("Brak kategorii. Dodaj najpierw kategorie.");

                        break;
                    }
                    System.out.println("Wpisz kategorię z listy: \n" + categories);
                    simpleCat.setCat(inputData());
                    categories.deleteCategory(simpleCat.getCat());
                    break;
                }
                case 6: {
                    if(productList.getProductList().isEmpty()) System.out.println("Brak produktów. ");
                    System.out.println("Nazwa produktu: ");
                    String name=inputData();
                    productList.deleteProduct(name);
                    break;
                }
                default:{
                    if(programRunning!=7) System.out.println("Brak dostępnej opcji");
                }
            }
        } while(programRunning!=7);
    }

}
