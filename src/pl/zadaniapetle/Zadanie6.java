package pl.zadaniapetle;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        int declaredNumber=600;
        int usersGuess=0;
        Scanner scanner = new Scanner(System.in);
        while(usersGuess!=declaredNumber) {
            usersGuess=scanner.nextInt();
            if(usersGuess<declaredNumber) System.out.println("Liczba jest za mała.");
            else if (usersGuess>declaredNumber) System.out.println("Liczba jest za duża.");
        }
        System.out.println("Zgadłeś! Liczba to: " + declaredNumber);
    }
}
