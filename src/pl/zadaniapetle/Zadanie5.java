package pl.zadaniapetle;

import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Wprowadź liczbę: ");
        int n=scanner.nextInt();
        int sum=0;
        for(int i=1; i<=n; i++) {
            sum+=i;
        }
        System.out.println("Suma liczb od 1 do n wynosi: "+sum);
    }
}
