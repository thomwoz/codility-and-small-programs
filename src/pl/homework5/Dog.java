package pl.homework5;

public class Dog {

    private String name;
    private int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void bark(int howManyTimes) {
        String barking;
        if(age<5)
        barking="hau";
        else if(age>=5&&age<10)
        barking="Grr Hau";
        else barking="Hauuu";

        for(int i=0; i<howManyTimes; i++) {
            System.out.println(barking);
        }
    }
}
