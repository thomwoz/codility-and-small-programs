package pl.homework5;

public class DogTest {
    public static void main(String[] args) {
        Dog dog1=new Dog("Ratler",2);
        Dog ratler=new Dog("Mendek",5);
        Dog germanshephard=new Dog("Józek",12);

        dog1.bark(3);
        ratler.bark(2);
        germanshephard.bark(6);

    }
}
