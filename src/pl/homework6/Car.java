package pl.homework6;

public class Car {

    private String marka;
    private String model;
    private int power;
    private double timeToReach100;

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public double getTimeToReach100() {
        return timeToReach100;
    }

    public void setTimeToReach100(double timeToReach100) {
        this.timeToReach100 = timeToReach100;
    }

    public Car(String marka, String model, int power, double timeToReach100) {
        this.marka = marka;
        this.model = model;
        this.power = power;
        this.timeToReach100 = timeToReach100;
    }

    @Override
    public String toString() {
        return "Car{" +
                "marka='" + marka + '\'' +
                ", model='" + model + '\'' +
                ", power=" + power +
                ", timeToReach100=" + timeToReach100 +
                '}';
    }
}
