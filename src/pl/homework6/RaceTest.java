package pl.homework6;

public class RaceTest {
    public static void main(String[] args) {
        Race race=new Race();
        race.addCar(new Car("WV", "Golf", 123, 9.43));
        race.addCar(new Car("BMW", "Mini", 123, 4.5));
        race.addCar(new Car("WV", "Passat", 123, 6.87));

        race.decideWinner();
        System.out.println(race.getWinner());
        System.out.println("\n\n");
        race.showParticipants();
    }
}
