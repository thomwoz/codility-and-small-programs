package pl.homework6;

import java.util.ArrayList;
import java.util.List;

public class Race {

    private Car winner;
    private List<Car> carList;

    public Car getWinner() {
        return winner;
    }

    public Race() {
        carList=new ArrayList<>();
    }

    public void addCar(Car car){
        carList.add(car);
    }

    public void showParticipants() {
        for(Car car:carList) System.out.println(car);
    }

    public void decideWinner(){
        Car winner=carList.get(0);
        for(Car car: carList){
            if(winner.getTimeToReach100()>=car.getTimeToReach100()) winner=car;
        }
        this.winner=winner;
    }
}
