package pl.paczkikurierzy;

import java.util.ArrayList;
import java.util.List;

public class CompanyLogic {
    private List<Courier> couriers;
    private List<Package> allPackages;

    public CompanyLogic() {
        this.couriers = new ArrayList<>();
        this.allPackages = new ArrayList<>();
    }

    public List<Courier> getCouriers() {
        return couriers;
    }

    public void addCourier(String idRegion){
        couriers.add(new Courier(idRegion));
    }

    public void addPackage(String address){
        allPackages.add(new Package(address));
    }

    public void addPackage(String address, String regionId){
        Package p=new Package(address,regionId);
        allPackages.add(p);
        for(Courier c:couriers) {
            if(c.getRegionId().equals(p.getRegionId())){
                c.getPackageList().add(p);
            }
        }
    }

    public void showPackagesWithoutIdRegion() {
        for(Package p:allPackages){
            if(p.getStatus()==Status.ZAREJESTROWANA)
            System.out.println(p);
        }
    }

    public void showAllPackages(Courier courier) {
        System.out.println(courier);
        for(Package p:courier.getPackageList()){
                System.out.println(p);
        }
    }

    public void showAllCouriers(){
        for(Courier c:couriers){
            System.out.println(c);
        }
    }

}
