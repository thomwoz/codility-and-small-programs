package pl.paczkikurierzy;

import java.util.ArrayList;
import java.util.List;

public class Package implements Comparable<Package> {
//    private static List<Package> allPackages=new ArrayList<>();

    private String address;
    private String regionId;
    private Status status;

    public Package(String address) {
        this.address = address;
        regionId="";
        status=Status.ZAREJESTROWANA;
    }

    public Package(String address, String regionId) {
        this.address = address;
        this.regionId = regionId;
        status=Status.DONADANIA;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int compareTo(Package o) {
        if(this.getStatus()==Status.ZAREJESTROWANA && !(o.getStatus()==Status.ZAREJESTROWANA))return -1;
        if(!(this.getStatus()==Status.ZAREJESTROWANA) && o.getStatus()==Status.ZAREJESTROWANA) return 1;
        return 0;
    }

    @Override
    public String toString() {
        return "Package{" +
                "address='" + address + '\'' +
                ", regionId='" + regionId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

}
