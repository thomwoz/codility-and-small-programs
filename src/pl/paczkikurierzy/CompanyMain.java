package pl.paczkikurierzy;



public class CompanyMain {
    public static void main(String[] args) {
        CompanyLogic companyLogic=new CompanyLogic();

        companyLogic.addCourier("ASD123");
        companyLogic.addCourier("QWE456");

        companyLogic.addPackage("Warsaw 01/43");
        companyLogic.addPackage("Lodz 11/34");
        companyLogic.addPackage("Warsaw 11/54", "ASD123");
        companyLogic.addPackage("Warsaw 65/554", "QWE456");
        companyLogic.addPackage("Warsaw 18/33", "ASD123");
        companyLogic.addPackage("Warsaw 12/33", "ASD123");
        companyLogic.addPackage("Warsaw 12/58", "QWE456");

        companyLogic.showAllPackages(companyLogic.getCouriers().get(0));
        companyLogic.showAllPackages(companyLogic.getCouriers().get(1));


        System.out.println(        );
        System.out.println();

        companyLogic.showPackagesWithoutIdRegion();

    }
}
