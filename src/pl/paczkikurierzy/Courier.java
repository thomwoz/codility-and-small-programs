package pl.paczkikurierzy;

import java.util.ArrayList;
import java.util.List;

public class Courier {
    private List<Package> packageList;
    private String regionId;

    public List<Package> getPackageList() {
        return packageList;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public Courier(String regionId) {
        this.packageList = new ArrayList<>();
        this.regionId = regionId;
    }

    @Override
    public String toString() {
        return "Courier{" +
                "regionId='" + regionId + '\'' +
                '}';
    }
}
