package pl.klasypodstawy;


import pl.produktykategorie.Category;
import pl.zadaniapowtorkowe2.Zadanie2;

public class Mem {

    public String name;
    public String url;
    public String description;
    public boolean isFavourite;
    public String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Mem(String name, String url, String description, boolean isFavourite) {
        this.name=name;
        setUrl(url);
        this.description=description;
        this.isFavourite=isFavourite;
    }

    public void setUrl(String url) {
        if(Zadanie2.isURL(url)) this.url = url;
        else System.out.println("Wprowadziłeś błędny adres URL.");
    }

    public String getUrl() {
        if(url==null) return "http://???";
        return url;
    }

    public void addCategory(){
        System.out.println("Wprowadź kategorię: ");

    }

    @Override
    public String toString() {
        return name + " - " + getUrl() + " - "+ description;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

}
