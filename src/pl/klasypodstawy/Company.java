package pl.klasypodstawy;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private String name;
    private String address;
    private int numberOfEmployees;
    private List<Employee> employeeList=new ArrayList<>();

    public Company(){
    }

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
        this.employeeList = employeeList;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfEmployees(){
        return employeeList.size();
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public void addEmployee(Employee employee) {
        getEmployeeList().add(employee);
    }

    @Override
    public String toString() {
        StringBuilder s= new StringBuilder("Company{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", numberOfEmployees=" + numberOfEmployees + '\n' + "Pracownicy: " + "\n");

                for(Employee e:getEmployeeList()) {
                    s.append("- "+e);
                }
                return s.toString();
    }
}
