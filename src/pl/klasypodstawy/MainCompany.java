package pl.klasypodstawy;

public class MainCompany {
    public static void main(String[] args) {
        Company company=new Company("RURURU", "Warszawa os. Jakieś 88/13");
        company.addEmployee(new Employee("Jan", "Kowalski", 34));
        company.getEmployeeList().get(0).setSalary(3456.33);

        System.out.println(company);
    }
}
