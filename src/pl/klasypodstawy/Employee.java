package pl.klasypodstawy;

public class Employee {

    private String firstName;

    private String lastName;

    private double salary;

    private int age;

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", salary=" + salary +
                ", age=" + age +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (isCorrectName(firstName))
            this.firstName = firstName;
        else
            System.out.println("Wprowadziłeś nieprawidłowe imię");
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (isCorrectName(lastName))
            this.lastName = lastName;
        else
            System.out.println("Wprowadziłeś nieprawidłowe nazwisko");
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        if (salary > 0)
            this.salary = salary;
        else
            System.out.println("Wprowadziłeś niepoprawne dane");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Employee(String firstName, String lastName) {
        setFirstName(firstName);
        setLastName(lastName);
    }

    public Employee(String firstName, String lastName, double salary) {
        this(firstName, lastName);
        setSalary(salary);
    }

    public Employee(String firstName, String lastName, int age) {
        this(firstName, lastName);
        this.age = age;
    }

    public double annualSalary() {
        return 12 * salary;
    }

    private boolean isCorrectName(String name) {
        return (name.length() >= 2 && Character.isUpperCase(name.charAt(0)));
    }


}