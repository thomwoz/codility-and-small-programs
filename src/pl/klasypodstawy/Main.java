package pl.klasypodstawy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        List<Mem> memList = new ArrayList<>();
        memList.add(new Mem("Mem1", "http://zupa.pl", "dsa dsa wwww dfds", true));
        memList.add(new Mem("Mem2", "http://zupa.pl", "dsa dsa wwww dfds", false));
        memList.add(new Mem("Mem3", "http://zupa.pl", "dsa dsa wwww dfds", true));
        memList.add(new Mem("Mem4", "hwttp://zupa.pl", "dsa dsa wwww dfds", false));
        memList.add(new Mem("Mem5", "http://zupa.pl", "dsa dsa wwww dfds", true));
        memList.add(new Mem("Mem6", "http://zupa.pl", "dsa dsa wwww dfds", false));
        memList.add(new Mem("Mem7", "hzttp://zupa.pl", "dsa dsa wwww dfds", true));

        for(Mem mem: memList){
            if(mem.isFavourite()) System.out.println(mem);
        }
    }
}
