package pl.zadaniapowtorkowe2;

import java.util.Scanner;

public class Zadanie2 {


    public static boolean isURL(String url){
        boolean isURL=false;
        url=url.toLowerCase();
            if(url.startsWith("https://www.")){
                if(((url.substring(12).split("\\.").length)>0) && !hasWrongSigns(url.substring(12))) {
                    isURL=true;
                }
        }
        else if(url.startsWith("http://www.")){
                if((url.substring(11).split("\\.").length>0) && !hasWrongSigns(url.substring(11))){
                    System.out.println("jest kropka");
                    isURL=true;
                }
        }
        else if(url.startsWith("www.")) {
            if(((url.substring(4).split("\\.").length)>0) && !hasWrongSigns(url.substring(4))) {
                isURL=true;
            }
            }
        return isURL;
    }

    private static boolean hasWrongSigns(String string){
        boolean hasWrong=false;
        for(int i=0; i<string.length(); i++) {
            if ((int)string.charAt(i) <= 45 || ((int)string.charAt(i) == 47) ||((int)string.charAt(i) >= 58 && (int)string.charAt(i) <= 64) ||
                    ((int)string.charAt(i) >= 91 && (int)string.charAt(i) <= 96) || (int)string.charAt(i) >= 123) {
                hasWrong = true;
                break;
            }
        }
        return hasWrong;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj adres www: ");

        String url=scanner.nextLine();

        if(isURL(url)) System.out.println("Poprawny adres!");
        else System.out.println("Adres jest niepoprawny!");



    }
}
