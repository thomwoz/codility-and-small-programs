package pl.zadaniapowtorkowe2;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String word=scanner.nextLine();

        if(Character.isUpperCase(word.charAt(0))) System.out.println("Zaczyna się z wielkiej litery.");
        else System.out.println("Nie zaczyna się!");
    }
}
