package pl.zadaniapowtorkowe2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Zadanie1 {

    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        Random random= new Random();
        for(int i=0; i<6; i++) {
            int a=random.nextInt(51+1);
            while(set.contains(a)) {
                a=random.nextInt(51+1);
            }
            set.add(a);
            System.out.println("Liczba nr " + i + " to "+a);
        }
    }
}
