package pl.klasy2Zadanie9;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String username,password,name,surname;

    private static List<User> users=new ArrayList<>();

    public User(String username, String password, String name, String surname) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        users.add(this);
    }

    public static List<User> getAll(){
        return users;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
